#!/bin/bash
#make sure do have access to docker, if not set correct rights or use root user
cd base
docker login registry.gitlab.com/veloren/veloren-docker-ci;
time docker build -t registry.gitlab.com/veloren/veloren-docker-ci:base --no-cache .;
docker push registry.gitlab.com/veloren/veloren-docker-ci:base;
cd ../cache
docker login registry.gitlab.com/veloren/veloren-docker-ci;
time docker build -t registry.gitlab.com/veloren/veloren-docker-ci:latest --no-cache .;
docker push registry.gitlab.com/veloren/veloren-docker-ci;
