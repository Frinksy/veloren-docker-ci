
pwdpath=$(pwd)
if [[ $(basename $pwdpath) != "runner" ]]; then
  echo "you must cd into the ./runner/ directory before executing this script"
  exit 1;
fi

read -p "Prune old images? Choose no if you have slow internet and much hdd space left or other docker images you want to keep (y/n)?" prunechoice
case "$prunechoice" in
  y|Y|n|N ) echo " >>> choice stored";;
  * ) echo "invalid" && exit 1;;
esac

echo " >>> updating repo" &&
git pull &&
echo " >>> download latest gitlab-runner" &&
docker pull gitlab/gitlab-runner:latest &&

case "$prunechoice" in
  y|Y ) echo " >>> pruning images" && docker system prune -a --volumes;;
  n|N ) echo " >>> skip pruning";;
  * ) echo "invalid";;
esac &&

echo " >>> printing all running containers: " &&
docker container list &&
echo " >>> please list the [CONTAINER ID] from your gitlab-runner from the listing above (e.g. 1234ab567c8d)"
read -p "CONTAINER ID: " CONTAINER_ID &&
echo " >>> stopping and removing old container" &&
docker container stop "${CONTAINER_ID}" &&
docker container rm "${CONTAINER_ID}" &&
echo " >>> removal complete, running install script" &&
source launch-runner.sh &&
echo " >>> update complete"
