pwdpath=$(pwd)
if [[ $(basename $pwdpath) != "runner" ]]; then
  echo "you must cd into the ./runner/ directory before executing this script"
  exit 1;
fi

echo "Please enter Veloren's Gitlab runner token. It can be found at https://gitlab.com/veloren/veloren/settings/ci_cd"
read -p "Token: " GITLAB_RUNNER_TOKEN

echo "\nWhat should this runner be called?"
read -p "Name: " GITLAB_RUNNER_NAME

# Start the runner
docker run -d --security-opt seccomp=unconfined --name "$GITLAB_RUNNER_NAME" --restart always \
  -v /srv/"$GITLAB_RUNNER_NAME"/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest

retVal=$?
if [ $retVal -ne 0 ]; then
  echo "There was a error setting up the gitlab-runner docker container, exiting"
  exit 2;
fi
echo "register the specific runner"

# Register the runner
docker run --rm -t -i \
  -v /srv/"$GITLAB_RUNNER_NAME"/config:/etc/gitlab-runner \
  -v "$(pwd)"/template.toml:/srv/template.toml \
  gitlab/gitlab-runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image registry.gitlab.com/veloren/veloren-docker-ci \
  --url "https://gitlab.com/" \
  --registration-token "$GITLAB_RUNNER_TOKEN" \
  --description "$GITLAB_RUNNER_NAME" \
  --tag-list "veloren-docker" \
  --run-untagged="true" \
  --locked="false" \
  --template-config /srv/template.toml
