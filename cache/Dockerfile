FROM registry.gitlab.com/veloren/veloren-docker-ci:base

ARG RUST_TOOLCHAIN=nightly-2021-01-01
ARG PROJECTNAME=veloren

RUN cd /dockercache; \
    mkdir /dockercache/cache-all; \
    mkdir /dockercache/cache-release-linux; \
    mkdir /dockercache/cache-release-windows; \
    mkdir /dockercache/cache-release-macos; \
    mkdir /dockercache/cache-tarpaulin; \
    git clone "https://gitlab.com/veloren/${PROJECTNAME}.git"; \
    cd "/dockercache/${PROJECTNAME}"; \
    git lfs install; \
    git lfs fetch --recent; \
    git checkout master; \
    git lfs pull; \
    sleep 10; \
    echo ${RUST_TOOLCHAIN} > ./rust-toolchain; \
    . /root/.cargo/env; \
    export RUSTFLAGS="-D warnings"; \
    ln -s /dockercache/cache-all /dockercache/veloren/target; \
    cargo check --locked; \
    cargo check --examples --locked; \
    cargo clippy -- --warn clippy::all; \
    cargo clippy --tests -- --warn clippy::all; \
    cargo audit; \
    cargo fmt --all -- --check; \
    cargo test; \
    cargo bench; \
    cargo build; \
    rm /dockercache/veloren/target; \
    \
    ln -s /dockercache/cache-tarpaulin /dockercache/veloren/target; \
    cargo tarpaulin -v; \
    cargo tarpaulin -v; \
    cargo tarpaulin -v; \
    rm /dockercache/veloren/target; \
    \
    ln -s /dockercache/cache-release-linux /dockercache/veloren/target; \
    VELOREN_ASSETS=assets cargo build --release; \
    rm /dockercache/veloren/target; \
    \
    ln -s /dockercache/cache-release-windows /dockercache/veloren/target; \
    VELOREN_ASSETS=assets cargo build --target=x86_64-pc-windows-gnu --release; \
    rm /dockercache/veloren/target; \
    \
    ln -s /dockercache/cache-release-macos /dockercache/veloren/target; \
    VELOREN_ASSETS=assets WINIT_LINK_COLORSYNC=true PATH="/dockercache/osxcross/target/bin:$PATH" COREAUDIO_SDK_PATH=/dockercache/osxcross/target/SDK/MacOSX10.13.sdk CC=o64-clang CXX=o64-clang++ cargo build --target=x86_64-apple-darwin --release; \
    rm /dockercache/veloren/target; \
    sleep 10;
